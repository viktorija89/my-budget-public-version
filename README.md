My Budget
===============

A Symfony project created on June 15, 2015, 1:41 pm.

Čia mano šiuo metu rašoma sistema. Veikia registracija, registracijos formos validacija, vartotojo prisijungimas, 
slaptažodžio kodavimas, du menu tabai (išlaidos ir pajamos), galima įvesti išlaidas, pajamas pagal atitinkamas 
kategorijas, jas išsaugoti duomenų bazėje (MySQL). 
Savaitinės ir mėnesinės išlaidų sumos atvaizduojamos
naudojant Ajax, sistema pritaikyta skirtingo dydžio ekranams (screenshot apačioje). 

Rašau su PHP (Symfony2), naudojau Bootstrap, jQuery, Ajax. Sistema dar nebaigta, kadangi mokytis programuoti 
pradėjau nuo 2015 m. sausio, baigtų pavyzdžių kol kas neturiu.

Registracijos puslapis:
------------------
![Registration page](/ReadmeImages/register.PNG)

Registracijos puslapis (mobiliajame):
------------------
![Registration page mobile](/ReadmeImages/registerMobile.PNG)

Login puslapis:
------------------
![Login page](/ReadmeImages/login.PNG)

Login puslapis (mobiliajame):
------------------
![Login page mobile](/ReadmeImages/loginMobile.PNG)

Išlaidų puslapis:
------------------
![Expenses page](/ReadmeImages/expenses.PNG)

Pajamų puslapis (mobiliajame):
------------------
![Income page mobile](/ReadmeImages/incomeMobile.PNG)
